class Room {
  constructor (capacity, id) {
    this._capacity = capacity
    this._id = id
    this._players = []
    this._openPlayers = []
    this._readyPlayers = []
  }

  addPlayer (player) {
    this._players.push(player)
  }

  removePlayer (player) {
    const pIndex = this._players.indexOf(player)
    if (pIndex < 0) return
    this._players.splice(pIndex, 1)

    const rIndex = this._openPlayers.indexOf(player)
    if (rIndex < 0) return
    this._openPlayers.splice(rIndex, 1)
  }

  forEach (callback) {
    this._players.forEach(callback)
  }

  setPlayerOpen (player) {
    if (!this._players.includes(player)) return
    if (!this._openPlayers.includes(player)) {
      this._openPlayers.push(player)
    }
  }

  setPlayerReady (player) {
    if (!this._players.includes(player)) return
    if (!this._readyPlayers.includes(player)) {
      this._readyPlayers.push(player)
    }
  }

  allPlayersOpen () {
    return this._openPlayers.length === this._capacity
  }

  allPlayersReady () {
    return this._readyPlayers.length === this._capacity
  }

  getSize () {
    return this._players.length
  }

  getId () {
    return this._id
  }
}

module.exports = Room
