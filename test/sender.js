const ws = require('./connection')

ws.on('open', () => {
  sendText()
  sendJSON()
  sendBinary()
})

function sendText () {
  ws.send('text message test')
}

function sendJSON () {
  const body = { body: { data: 'JSON message test' } }
  ws.send(JSON.stringify(body))
}

/**
 * @see https://www.npmjs.com/package/ws
 */
function sendBinary () {
  const array = new Float32Array(5)

  for (let i = 0; i < array.length; ++i) {
    array[i] = i / 2
  }

  ws.send(array)
}
