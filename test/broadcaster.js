const WebSocket = require('ws')
const wss = require('./server')

// Broadcast to all.
wss.broadcast = data => {
  wss.clients.forEach(function each (client) {
    if (client.readyState === WebSocket.OPEN) {
      client.send(data)
    }
  })
}

// Broadcast to everyone else.
wss.on('connection', ws => {
  ws.on('message', data => {
    wss.clients.forEach(client => {
      if (client !== ws && client.readyState === WebSocket.OPEN) {
        client.send(data)

        wss.broadcast(data)
      }
    })
  })
})
