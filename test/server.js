const WebSocket = require('ws')

const wss = new WebSocket.Server({ port: 3000 })
  .on('listening', () => {
    console.log(`test server: listening on port ${wss.options.port}`)
  })

wss.on('connection', (ws, req) => {
  console.log(`test server: client connected: ${req.connection.remoteAddress}`)

  ws.on('message', function incoming (data) {
    console.log(`received: ${data}`)
  })
})

module.exports = wss
