const express = require('express')
const app = express()
const http = require('http')
const server = http.createServer(app)
const WebSocket = require('ws')
const Room = require('./room')
const Player = require('./player')
const utils = require('./utils')

const ROOM_CLIENTS = 2

const listener = server.listen(process.env.PORT || 3000)

const wss = new WebSocket.Server({ server })
  .on('listening', () => {
    console.log(`listening on port ${listener.address().port}`)
  })

const rooms = []

function noop () {
}

function heartbeat () {
  this.isAlive = true
}

setInterval(function ping () {
  wss.clients.forEach(ws => {
    if (ws.isAlive === false) return ws.terminate()

    ws.isAlive = false
    ws.ping(noop)
  })
}, 2000)

// Broadcast to all.
wss.broadcast = data => {
  wss.clients.forEach(client => {
    if (client.readyState === WebSocket.OPEN) {
      client.send(data)
    }
  })
}

wss.on('connection', (ws, req) => {
  ws.isAlive = true
  ws.on('pong', heartbeat)

  let room
  if (rooms.length < 1) {
    room = new Room(ROOM_CLIENTS, utils.getId())
    rooms.push(room)
  } else {
    room = rooms[0]
    rooms.splice(0, 1)
  }
  let player = new Player(ws, room)

  console.log(`player ${player.getNumber()} connected to room ${player.getRoom().getId()}: ${req.connection.remoteAddress}`)

  ws.on('close', () => onClose(ws, player))
  ws.on('message', message => onMessage(ws, player, message))
})

function onClose (ws, player) {
  const room = player.getRoom()
  const roomIndex = rooms.indexOf(room)
  if (roomIndex >= 0) rooms.splice(roomIndex, 1)

  console.log(`player ${player.getNumber()} has left the room`)

  room.removePlayer(player)
  room.forEach(player => {
    const client = player.getClient()
    client.send(JSON.stringify({ event: 'leave', player: player.getNumber() }))
  })
  if (room.getSize() < 2) {
    room.forEach(player => {
      player.getClient().close()
    })
  }
}

function onMessage (ws, player, message) {
  let room = player.getRoom()

  switch (message) {
    case 'open':
      setOpen(player, room)
      break
    case 'ready':
      setReady(player, room)
      break
    case 'finish':
      finish(ws, player, room)
      break
    case 'destroy':
      destroy(ws, player, room)
      break
    default:
      updatePlayers(ws, player, room, message)
  }
}

function setOpen (player, room) {
  room.setPlayerOpen(player)
  if (room.allPlayersOpen()) {
    room.forEach(player => {
      const client = player.getClient()
      if (client.readyState === WebSocket.OPEN) {
        client.send(JSON.stringify({ event: 'ready', player: player.getNumber() }))
      }
    })
  }
}

function setReady (player, room) {
  room.setPlayerReady(player)
  if (room.allPlayersReady()) {
    room.forEach(player => {
      const client = player.getClient()
      client.send(JSON.stringify({ event: 'start' }))
    })
  }
}

function updatePlayers (ws, player, room, message) {
  room.forEach(player => {
    let client = player.getClient()
    if (client !== ws) {
      client.send(message)
    }
  })
}

function finish (ws, player, room) {
  room.forEach(player => {
    const client = player.getClient()
    if (client !== ws) {
      client.send(JSON.stringify({ event: 'finish', player: player.getNumber() }))
      client.close()
    }
  })
  player.getClient().close()
}

function destroy (ws, player, room) {
  room.forEach(player => {
    const client = player.getClient()
    if (client !== ws) {
      client.send(JSON.stringify({ event: 'destroy', player: player.getNumber() }))
      client.close()
    }
  })
  player.getClient().close()
}
