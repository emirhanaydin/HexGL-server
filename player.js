class Player {
  constructor (client, room) {
    this._client = client
    this._room = room
    this._number = room.getSize()

    this._room.addPlayer(this)
  }

  getClient () {
    return this._client
  }

  getRoom () {
    return this._room
  }

  getNumber () {
    return this._number
  }
}

module.exports = Player
